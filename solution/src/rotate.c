#include "../include/rotate_image.h"

#include <malloc.h>

struct image rotate(struct image const* source) {
    struct image result;
    result.pixels = malloc(sizeof(struct pixel) * source->width * source->height);
    result.height = source->width;
    result.width = source->height;

    for (size_t i = 0; i < source->width; i++) {
        for (size_t j = 0; j < source->height; j++) {
            result.pixels[i * result.width + j] = source->pixels[(source->height - 1 - j) * source->width + i];
        }
    }

    return result;
}

