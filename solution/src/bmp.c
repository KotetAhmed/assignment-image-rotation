#include "../include/bmp_header.h"
#include "../include/bmp_util.h"

#include <assert.h>
#include <malloc.h>
#include <stdio.h>

#define BMP_TYPE 0x4D42
#define BMP_PLANES 1
#define BMP_HEADER_SIZE 40

enum read_status read_header(FILE* in, struct bmp_header* header) {
	// size_t size = ftell(in);
	if (fread(header, sizeof(struct bmp_header), 1, in)) {
		return READ_OK;
	}
	else
	{
		return READ_INVALID_HEADER;
	}
}

enum read_status read_pixels(struct image* img, FILE* in, uint8_t padding) {
	size_t width;
	size_t height;
	width= img->width;
	height = img->height;

	struct pixel* pixels = malloc(width * height * sizeof(struct pixel));
	if (pixels == NULL){
		return READ_PIXELS_ERROR;
	}

	for (size_t i = 0; i < height; i++) {
		if (pixels + i * width != 0)
		{
			fread(pixels + i * width, sizeof(struct pixel), width, in);
			fseek(in, padding, SEEK_CUR);
		}
	}
	img->pixels = pixels;
	return READ_OK;
}



uint8_t get_padding(uint32_t width) {
	if (width % 4 != 0){
	return ((4 - width * sizeof(struct pixel)) % 4);
	} else {
		return 0;
	}
}

enum read_status from_bmp(FILE* in, struct image* img) {
	if (in != NULL && img != NULL){
	struct bmp_header header = { 0 };
	if (read_header(in, &header) == READ_INVALID_HEADER) {
		return READ_INVALID_SIGNATURE;
	}
	img->width = header.biWidth;
	img->height = header.biHeight;
	return read_pixels(img, in, get_padding(header.biWidth));
	} else {
		return 0;
	}
}

struct bmp_header create_header(const struct image* img) {
	uint32_t img_size = (sizeof(struct pixel) * img->width + get_padding(img->width)) * img->height;
	struct bmp_header header = {
		.bfType = BMP_TYPE,
		.bfileSize = sizeof(struct bmp_header) + img_size,
		.bfReserved = 0,
		.bOffBits = sizeof(struct bmp_header),
		.biSize = BMP_HEADER_SIZE,
		.biWidth = img->width,
		.biHeight = img->height,
		.biPlanes = BMP_PLANES,
		.biBitCount = 24,
		.biCompression = 0,
		.biSizeImage = img_size,
		.biXPelsPerMeter = 0,
		.biYPelsPerMeter = 0,
		.biClrUsed = 0,
		.biClrImportant = 0,

	};
	return header;
}



enum write_status write_image(struct image const* img, FILE* out, uint8_t padding) {
	size_t width = img->width;
	size_t height = img->height;
	uint64_t zero = 0;
	uint64_t cnt = 0;
	for (size_t i = 0; i < height; ++i) {
		cnt += fwrite(img->pixels + i * width, sizeof(struct pixel), width, out);
		fwrite(&zero, 1, padding, out);
	}
	if (cnt == height * width) {
		return WRITE_OK;
	}
	return WRITE_ERROR;
}

enum write_status to_bmp(FILE* out, struct image* img) {
	if (out != NULL && img != NULL){
	struct bmp_header header = create_header(img);
	if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) {
		return WRITE_ERROR;
	}
	uint8_t padding = get_padding(header.biWidth);
	return write_image(img, out, padding);
	} else {
		return 0;
	}
}
