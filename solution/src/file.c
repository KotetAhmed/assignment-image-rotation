#include "../include/file_util.h"

bool openf(FILE** file, const char* name, const char* mode) {
    if  (file != NULL && name != NULL && mode != NULL) {
        *file = fopen(name, mode);
        if (*file != NULL) return true;
    }
    return false;
}

bool closef(FILE** file) {
    if (file != NULL) {
        return !fclose(*file);
    }
    return false;
}
