#include "image.h"

#include <inttypes.h>
#include <malloc.h>

void image_destroy (struct image* img) {
    free(img->pixels);
}
