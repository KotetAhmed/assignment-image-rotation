#include "../include/rotate_image.h"
#include "../include/bmp_util.h"
#include "../include/file_util.h"
#include "../include/image.h"

#include <assert.h>
#include <malloc.h>
#include <stdio.h>

int main(int args, char** argv) {
	FILE* in = NULL;
	FILE* out = NULL;


	if (args != 3) 
	{
		fprintf(stderr, "Nevernoe kol-vo argumentov\n");
		return 1;
	}
	char* fname_input = argv[1];
	char* fname_output = argv[2];

	if (!openf(&in, fname_input, "rb") || !openf(&out, fname_output, "wb")) {
		fprintf(stderr, "Proizoshla oshibka pri chtenii fayla\n");
		closef(&in);
        closef(&out);
		return 1;
	}
	struct image image = { 0 };
	if (from_bmp(in, &image) != READ_OK) {
		fprintf(stderr, "Proizoshla oshibka pri chtenii fayla!\n");
		closef(&in);
        closef(&out);
        image_destroy(&image);
		return 1;
	}
	struct image rotated_image = rotate(&image);
	if (to_bmp(out, &rotated_image) != WRITE_OK) {
		fprintf(stderr, "Proizoshla oshibka pri zapisi v fayl\n");
        image_destroy(&image);
		image_destroy(&rotated_image);
        closef(&in);
		closef(&out);
		return 1;
	}
	if (!closef(&in) || !closef(&out)) {
		fprintf(stderr, "Ne poluchilos sakrit fayl\n");
		return 1;
	}
	image_destroy(&image);
	image_destroy(&rotated_image);
	return 0;
}

